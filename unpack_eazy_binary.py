'''
Code to read EAZY binary output files for P(z).

Currently reads first 1000 P(z) and makes a plot of the P(z) and a histogram
of the z_peak.
'''
import struct
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
from scipy.integrate import cumtrapz

'''
rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)
'''

plt.close('all') # tidy up any unshown plots


with open('./data-binary/photz_testky.pz', 'rb') as file:
  pz_filecontents = file.read()

nz, nobj = struct.unpack('ii', pz_filecontents[:8])

print(nobj)

# Only deal with first 1000 objects for now!
#nobj = 1000

chi2fit = struct.unpack('d'*nobj*nz, pz_filecontents[8:8+8*nobj*nz])
chi2fit = np.asarray(chi2fit)
chi2fit = chi2fit.reshape([nobj, nz])
pz = np.exp(-chi2fit.T/2)
z = np.linspace(0.01,6.00, nz)

with open('./data-binary/photz_testky.zbin', 'rb') as file:
  zbin_filecontents = file.read()

ncol = 5
nobj = struct.unpack('i', zbin_filecontents[:4])[0]
print(nobj)

data = struct.unpack('d'*nobj*ncol, zbin_filecontents[4:4+8*nobj*ncol])
data = np.asarray(data)
data = data.reshape([nobj, ncol])

z_a, z_p, z_m1, z_m2, z_peak = data.T

z_peak_good = z_peak[z_peak>0]
nz_arr = pz[:,z_peak>0].sum(axis=1)

plt.figure(1, figsize=(5, 3.75))
plt.hist(z_peak[z_peak>0], histtype='step', label='$z_{\\rm peak}$', normed=True, bins=50)
plt.plot(z, nz_arr/cumtrapz(nz_arr, z)[-1], '-', label='$n(z)$')
plt.legend(loc='upper right', frameon=False)
plt.xlabel('$\mathrm{Redshift} \, \, z$')
plt.savefig('plots/binary-example_nz.png', dpi=160, bbox_inches='tight')

zbins = np.linspace(0,6,4)
bin_idx = np.digitize(z_peak_good, zbins)

plt.figure(2, figsize=(10, 3.75))
plt.subplot(121)
plt.hist(z_peak_good[bin_idx==1], histtype='step', label='$0 < z_{\\rm peak} \leq 2$', normed=True)
plt.hist(z_peak_good[bin_idx==2], histtype='step', label='$2 < z_{\\rm peak} \leq 4$', normed=True)
plt.hist(z_peak_good[bin_idx==3], histtype='step', label='$4 < z_{\\rm peak} \leq 6$', normed=True)
#plt.legend(loc='upper left')
plt.legend(loc='upper left', frameon=False)
plt.xlabel('$\mathrm{Redshift} \, \, z$')
plt.subplot(122)
nz_bin1 = pz[:,z_peak>0][:,bin_idx==1].sum(axis=1)
nz_bin2 = pz[:,z_peak>0][:,bin_idx==2].sum(axis=1)
nz_bin3 = pz[:,z_peak>0][:,bin_idx==3].sum(axis=1)
plt.plot(z, nz_bin1, '-', label='$0 < z_{\\rm peak} \leq 2$')
plt.plot(z, nz_bin2, '-', label='$2 < z_{\\rm peak} \leq 4$')
plt.plot(z, nz_bin3, '-', label='$4 < z_{\\rm peak} \leq 6$')
plt.xlabel('$\mathrm{Redshift \, \, z}$')
plt.savefig('plots/binary-example_nz_binned.png', dpi=160, bbox_inches='tight')

plt.figure(3, figsize=(9, 3.75))
pz_galaxy5 = pz[:,z_peak>0][:,5]
pz_galaxy5 = pz_galaxy5/cumtrapz(pz_galaxy5, z)[-1]
pz_galaxy10 = pz[:,z_peak>0][:,10]
pz_galaxy10 = pz_galaxy10/cumtrapz(pz_galaxy10, z)[-1]
plt.subplot(121)
plt.plot(z, pz_galaxy5, 'k', label='$P(z)$')
plt.xlabel('$\mathrm{Redshift} \, \, z$')
plt.axvline(z_peak_good[5], linestyle='--', label='$z_{\\rm peak}$')
plt.legend(loc='upper left', frameon=False)
plt.subplot(122)
plt.plot(z, pz_galaxy10, 'k')
plt.axvline(z_peak_good[10], linestyle='--')
plt.xlabel('$\mathrm{Redshift} \, \, z$')
plt.savefig('plots/binary-example_pz.png', dpi=160, bbox_inches='tight')
