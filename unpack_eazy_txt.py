'''
Code to read EAZY txt output files for P(z).

Currently reads first 100 P(z) and makes a plot of the P(z) and a histogram
of the z_peak.
'''
import struct
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
from scipy.integrate import cumtrapz

from astropy.table import Table

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

cat = Table.read('data-txt/cat12_V1_photz.zout', format='ascii')

z_peak = cat['z_peak']
sig68 = abs(cat['l68'] - cat['u68'])

plt.figure(1, figsize=(10, 3.75))
plt.subplot(121)
ax = plt.gca()
ax.set_title('$\mathrm{Full \, \, Sample}$')
plt.hist(z_peak[z_peak>0], histtype='step', label='$z_{\\rm peak}$', normed=True, bins=50)
plt.hist(z_peak[(z_peak>0) & (sig68<1.0)], histtype='step', label='$\sigma_{68} < 1.0$', normed=True, bins=50)
plt.hist(z_peak[(z_peak>0) & (sig68<0.5)], histtype='step', label='$\sigma_{68} < 0.5$', normed=True, bins=50)
plt.legend(loc='upper right', frameon=False)
plt.xlabel('$\mathrm{Redshift} \, \, z$')
#plt.savefig('plots/example_nz_txt.png', dpi=300, bbox_inches='tight')

good_ids = cat['id'][(cat['z_peak'] > 0) & (cat['id'] < 101)]
z, chi2 = np.loadtxt('data-txt/cat12_V1_photz_100_1.0000000000.pz', unpack=True)
nz = np.zeros_like(z)

for idx in good_ids:

  z, chi2 = np.loadtxt('data-txt/cat12_V1_photz_100_{0}.0000000000.pz'.format(int(idx)), unpack=True)

  plt.figure(2, figsize=(5, 3.75))
  plt.clf()
  pz = np.exp(-chi2/2.)
  nz  = nz+pz
  plt.plot(z, pz, 'k-')
  plt.axvline(cat['z_peak'][cat['id']==idx], linestyle='--', label='$z_{\\rm peak}$')
  plt.axvline(cat['z_m1'][cat['id']==idx], linestyle='-', label='$z_{\\rm m1}$')
  plt.legend(frameon=False, loc='upper right')
  plt.text(4.75, 0.75*pz.max(), '$\sigma_{68} = $'+('%.2f' % (sig68[cat['id']==idx])))
  plt.xlabel('$\mathrm{Redshift} \, \, z$')
  plt.ylabel('$P(z)$')
  plt.savefig('plots/pz_plot_outputs/pz{0}_txt.png'.format(int(idx)), dpi=300, bbox_inches='tight')
  print(idx)


#plt.figure(3, figsize=(5, 3.75))
plt.figure(1)
plt.subplot(122)
ax = plt.gca()
ax.set_title('$\mathrm{First \, \,} 100$')
good_ids = (cat['z_peak'] > 0) & (cat['id'] < 101)
plt.hist(z_peak[good_ids], histtype='step', label='$z_{\\rm peak}$', normed=True, bins=50)
#plt.hist(z_peak[good_ids & (sig68<1.0)], histtype='step', label='$\sigma_{68} < 1.0$', normed=True, bins=50)
#plt.hist(z_peak[good_ids & (sig68<0.5)], histtype='step', label='$\sigma_{68} < 0.5$', normed=True, bins=50)
plt.plot(z, nz/cumtrapz(nz, z)[-1], '-', label='$n(z)$')
plt.legend(loc='upper right', frameon=False)
plt.xlabel('$\mathrm{Redshift} \, \, z$')
plt.savefig('plots/example_nz_100_txt.png', dpi=300, bbox_inches='tight')

plt.figure(3, figsize=(10, 3.75))
plt.subplot(121)
plt.hexbin(z_peak[z_peak>0], sig68[z_peak>0], cmap='gnuplot2_r', gridsize=50)
plt.xlabel('$z_{\\rm peak}$')
plt.ylabel('$\sigma_{68}$')
plt.xlim([0,6])
plt.ylim([0,5.2])
cbar = plt.colorbar()
cbar.set_label('$N_{\\rm gal}$')
plt.subplot(122)
plt.hexbin(z_peak[z_peak>0], sig68[z_peak>0], cmap='gnuplot2_r', bins='log', gridsize=50)
plt.xlabel('$z_{\\rm peak}$')
plt.ylabel('$\sigma_{68}$')
plt.xlim([0,6])
plt.ylim([0,5.2])
cbar = plt.colorbar(ticks=[0,1,2,3,4])
cbar.set_label('$N_{\\rm gal}$')
cbar.ax.set_yticklabels(['$10^{0}$', '$10^{1}$', '$10^{2}$', '$10^{3}$', '$10^{4}$'])

plt.savefig('plots/example_z_sig68_txt.png', dpi=300, bbox_inches='tight')

plt.figure(4, figsize=(10, 3.75))
plt.subplot(121)
plt.hexbin(z_peak[(z_peak>0)*(z_peak<1)], sig68[(z_peak>0)*(z_peak<1)], cmap='gnuplot2_r', gridsize=50)
plt.xlabel('$z_{\\rm peak}$')
plt.ylabel('$\sigma_{68}$')
plt.xlim([0,1])
plt.ylim([0,5.2])
cbar = plt.colorbar()
cbar.set_label('$N_{\\rm gal}$')
plt.subplot(122)
plt.hexbin(z_peak[(z_peak>0)*(z_peak<1)*(sig68<0.5)], sig68[(z_peak>0)*(z_peak<1)*(sig68<0.5)], cmap='gnuplot2_r', gridsize=50)
plt.xlabel('$z_{\\rm peak}$')
plt.ylabel('$\sigma_{68}$')
plt.xlim([0,1])
plt.ylim([0,0.6])
cbar = plt.colorbar()
cbar.set_label('$N_{\\rm gal}$')

plt.savefig('plots/example_z_sig68_cuts_txt.png', dpi=300, bbox_inches='tight')
